# Studio19 Magento2 Variant Plugin

This is a plugin for **Magento 2.x** (not Magento 1.x) that provides a feed of the product/variant data that is needed to power the Studio19 rental widget.  We have a separate plugin available for Magento 1.x.

After installing this plugin, the new feed will be available at `https://your-domain.com/studio19_variants`.  To enable paging, the feed accepts two optional parameters `top` and `skip`.  For example, to get the third page of products with 100 products per page `https://your-domain.com/studio19_variants?top=100&skip=200`.

Once setup, the Studio19 rental service will retrieve your latest product data daily, and use this product data to show the latest rental rates on your website.

## Installation

*NB. These steps may be different depending on your setup.  Eg, the prebuilt bitnami images (which we used to test this plugin) use `sudo bin/magento-cli` and `sudo composer` instead.*

### 1. Go to your Magento 2 installation.  Eg.

```sh
cd /path/to/your/magento2/installation
```

### 2. Config composer to use our git repository for this plugin (since it's not published)

```sh
composer config repositories.studio19-module-variants git https://bitbucket.org/datalive/studio19-magento2-plugin.git
```

### 3. Add the plugin using composer

```sh
composer require studio19/module-variants dev-master
```

### 4. Update Magento

*This may vary depending on your installation*

```sh
php bin/magento setup:upgrade
php bin/magento cache:clean
```

I also needed
```sh
php bin/magento setup:di:compile
```

### 5. Test

If installed correctly, `https://your-domain.com/studio19_variants?top=100` should contain a JSON representation of your top 100 products.  (Of cause, replace your-domain.com with your domain name)


